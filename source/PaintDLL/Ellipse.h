#pragma once
#include "Shape.h"
class CEllipse :
	public CShape
{
public:
	CEllipse();
	void draw(HDC hdc);
	void setData(pair<int, int> pSrc, pair<int, int> pDes);
	CShape* create(){ return new CEllipse; };
	static string className(){ return "ellipse"; };
	~CEllipse();
};

