#include "stdafx.h"
#include "ShapeFactory.h"


CShapeFactory::CShapeFactory()
{
	
}

CShape* CShapeFactory::Get(string name)
{
	if (name == CLine::className()) return new CLine;
	else if (name == CRectangle::className()) return new CRectangle;
	else if (name == CEllipse::className()) return new CEllipse;
	else if (name == CSquare::className()) return new CSquare;
	else if (name == CCircle::className()) return new CCircle;
}


CShapeFactory::~CShapeFactory()
{
}
