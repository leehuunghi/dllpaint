#pragma once

#if defined(MYPAINTDLLLIBRARY_EXPORTS)
#	define PAINTAPI		__declspec(dllexport)
#else
#	define PAINTAPI		__declspec(dllimport)
#endif


#include "Circle.h"
#include "Line.h"
#include "Rectangle.h"
#include "Square.h"
#include "Ellipse.h"

class PAINTAPI CShapeFactory
{
private:
	CShapeFactory();
public:
	static CShape* Get(string name);
	~CShapeFactory();
};

