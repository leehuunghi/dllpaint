#include "stdafx.h"
#include "Line.h"


CLine::CLine()
{
}

void CLine::draw(HDC hdc)
{
	MoveToEx(hdc, m_Src.first, m_Src.second, NULL);
	LineTo(hdc, m_Des.first, m_Des.second);
}

void CLine::setData(pair<int, int> pSrc, pair<int, int> pDes)
{
	m_Src = pSrc;
	m_Des = pDes;
}


CLine::~CLine()
{
}
