#include "stdafx.h"
#include "Circle.h"


CCircle::CCircle()
{
}

void CCircle::draw(HDC hdc)
{
	int radius = abs(m_Des.first - m_Src.first) < abs(m_Des.second - m_Src.second) ? abs(m_Des.first - m_Src.first) : abs(m_Des.second - m_Src.second);

	if (m_Des.second > m_Src.second && m_Des.first < m_Src.first)
	{
		m_Des.first = m_Src.first - radius;
		m_Des.second = m_Src.second + radius;
	}
	else if (m_Des.second<m_Src.second&&m_Des.first>m_Src.first)
	{
		m_Des.first = m_Src.first + radius;
		m_Des.second = m_Src.second - radius;
	}
	else if (m_Des.second > m_Src.second&&m_Des.first > m_Src.first)
	{
		m_Des.first = m_Src.first + radius;
		m_Des.second = m_Src.second + radius;
	}
	else
	{
		m_Des.first = m_Src.first - radius;
		m_Des.second = m_Src.second - radius;
	}
	Ellipse(hdc, m_Src.first, m_Src.second,m_Des.first,m_Des.second);
}

CCircle::~CCircle()
{
}
