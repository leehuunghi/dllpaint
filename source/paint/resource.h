//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by paint.rc
//
#define IDC_MYICON                      2
#define IDD_PAINT_DIALOG                102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDC_LINE                        104
#define IDM_EXIT                        105
#define IDC_RECTANGLE                   105
#define IDC_SQUARE                      106
#define IDI_PAINT                       107
#define IDC_ELLIPSE                     107
#define IDI_SMALL                       108
#define IDC_CIRCLE                      108
#define IDC_PAINT                       109
#define IDM_ABOUT1                      110
#define IDM_EXIT1                       111
#define IDR_MAINFRAME                   128
#define ID_TYPESHAPE_LINE               32771
#define ID_TYPESHAPE_RECTANGLE          32772
#define ID_TYPESHAPE_SQUARE             32773
#define ID_TYPESHAPE_ELLIPSE            32774
#define ID_TYPESHAPE_CIRCLE             32775
#define IDC_                            32776
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32777
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
